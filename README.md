# PILLAR OF HUMILIATION
Your hatred. Your suffering. Your unbalance. Your own Pillar of Humiliation.

你的怨恨。你的苦难。你的不平。你拥有的耻辱柱。

## INDEX

- [Details](./details)
- [Lists](./lists)
- [Wiki 索引](./wiki/index.md)

## RULES

**0.** As long as you are added to the PoH list, unless the action was a mistake, you are not able to uncall it.

一旦进入 PoH 列表，除非为情节过轻等导致的误判，不可撤销。

**1.** At least two records are needed, which will be saved in `details` folder.

进入列表需要两条及两条以上记录，存放在 `details` 中。

**2.** There might be some private information of the Humiliation, but COULD NOT delete in the most cases.

可能会有从一些途径获取到的、违规者的私人信息，在绝大多数情况下不予删除。

> 关于我们对于私人信息的处理方式，参见 [隐私信息保护](./wiki/PrivateInfomationProtect.md)

**3.** The PoH greylist is a list that helps us to decide whether the one has the condition to enter the PoH list, the items are usually not so harmful to someone else.

PoH 灰名单是一个帮助我们决定一个人是否有条件进入 PoH 列表的列表，通常灰名单条目对其它人没有太大威胁。

**4.** We will discuss whether a item has the condition to be moved from the greylist to the PoH list in Issues page to make PoH fairer.

我们将在 Issues 页面讨论条目是否有条件从灰名单进入 PoH 列表以使 PoH 更加公平。

**5. KEEP COOL.** 

### OUR BOTTOM LINE

**0.** Malicious abuse of others

恶意辱骂他人

**1.** Copying without permission

未经许可抄袭

**2.** Obtain ill-gotten gains

谋取不义之财

**3.** Other cases depending on discussion

其它讨论决定

More information, see [Range](./wiki/Range.md)  
更多信息，参见 [范围](./wiki/Range.md)

## UPLOAD
**Way 0.** [Open an issue](hhttps://gitlab.com/Project-PoH/Pillar-of-Humiliation/issues/new)

**Way 1.** [Tell Rachel](https://t.me/Rachel030219)

**Way 2.** [Go to our Telegram group](https://t.me/ProjectPoH)

**Way 3.** Fork it and pull request

**Way 4.** Send an email to our mailbox ProjectPoH@163.com

## WHERE TO USE
**Way 0.** Add this project to your website's blacklist

将这个项目提及的人添加到您网站的黑名单

**Way 1.** When you are going to trade with someone, check if him/her is in this project

当您将要进行交易时，检查他/她有没有处在本项目黑名单中

**Way 2.** And more…

还有更多……


### WE ONLY ADMIT THE POH FROM PROJECT POH IN GITLAB.
