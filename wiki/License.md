# 协议
本文档描述了 PoH 项目所使用的授权协议。

## 概述
PoH 中所有的文字内容使用 [CC BY-NC-ND 4.0 国际](https://creativecommons.org/licenses/by-nc-nd/4.0/deed.zh) 协议进行授权（包括主分支，灰名单， wiki ）。

当您在 PoH 发布其它非文字的内容时，表明您已经授权 PoH 展示该内容。

## 协议解释
您可以在概述中的链接中查看官方的中文解释，下面是非官方解释。

请注意：协议解释不是法律文本，请以法律文本为准。

### 您的权利
只要遵守一定的条件，您可以在任何介质以任何形式复制、发行该作品。只要您遵循协议条款，该权利就无法被许可人收回。

### 您要遵守的条件
#### BY - 署名
在共享过程中，您必须给出合适的作者名称并提供许可协议的链接。
例如：
>来自 Project PoH  
https://gitlab.com/Project-PoH  
使用 CC BY-NC-ND 4.0 国际 协议授权
https://creativecommons.org/licenses/by-nc-nd/4.0/deed.zh

#### NC - 非商业性使用
您不得将其用于商业目的。

#### ND - 禁止演绎
如果您改动了该作品，您不能分发修改后的版本。

-----
`version: 2017r1-testing`

