# The PoH of forums
#### 0x00 [威风凛凛77](http://www.oneplusbbs.com/home.php?mod=space&uid=1287373)

[![02](./forums-images/威风凛凛77/02.png)](./forums-images/威风凛凛)

> 注：剽窃 [iKirby](https://t.me/iKirby) (Telegram) 的成果，原文已被删除
>
> 附： [存档链接](https://web.archive.org/web/20170404074603/http://www.oneplusbbs.com/thread-3325490-1-1.html) 以及 iKirby [原帖链接](http://coolapk.com/feed/3004469)

#### 0x01 [小叶](http://coolapk.com/u/171030)

[![03](./forums-images/小叶/03.png)](./forums-images/小叶)

#### 0x02 [Ali](https://t.me/whwiwjsici)

[![01](./forums-images/Ali/01.png)](./forums-images/Ali)

> 注：恶意攻击许多网站，例如 [MAT BBS](https://mat.letitfly.me) 和 [MAXFOX](https://maxfox.me)
>
> Ali 其它的通信方式：
>
> [1661890877](https://www.v2ex.com/member/1661890877) on V2EX
>
> [Alisummer](https://github.com/Alisummer) on GitHub

#### 0x03 [奇葩小超](http://coolapk.com/u/800835)

[![01](./forums-images/奇葩小超/01.png)](./forums-images/奇葩小超)

> 注：已经无法找到 奇葩小超 原用户

#### 0x04 [ヤスミン先生](http://www.coolapk.com/u/451885)

[![01](./forums-images/ヤスミン先生/01.png)](./forums-images/ヤスミン先生)

#### 0x05 [liudongmiao](http://coolapk.com/u/466489)

[![04](./forums-images/liudongmiao/04.png)](./forums-images/liudongmiao)

